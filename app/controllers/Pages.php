<?php
  class Pages extends Controller{
    public function __construct(){
      
    }

    public function index(){

      $data = [
        'title' => 'Welcome',
        'description' => 'Simple social app based on verysimple mvc pattern. Users can log in and read, write posts.'
      ];
      $this->view('pages/index', $data);
    }

    public function about(){
      $data = [
        'title' => 'About Page',
        'description' => 'Simple app to share posst with '
      ];
      $this->view('pages/about', $data);
    }
  }