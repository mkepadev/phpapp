<?php 

  class Posts extends Controller {
    public function __construct(){

      $this->postModel = $this->model('Post');
      $this->userModel = $this->model('User');
    }
      

    public function index(){
      // get posts
      $posts = $this->postModel->getPosts();
      $countPosts = count($posts);

      $data = [
        'posts' => $posts,
        'countPosts' => $countPosts
      ];
      $this->view('posts/index', $data);
    }
    public function add(){
      if(isLoggedIn()){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
          // add posts
          // sanitaze POST array
          
          $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

          $data = [
            'title' => trim($_POST['title']),
            'body' => trim($_POST['body']),
            'user_id' => trim($_SESSION['user_id']),
            'title_err' => '',
            'body_err' => ''
          ];
          //validate title
          if(empty($data['title'])){
            $data['title_err'] = 'Please add title.';
          }
  
          //validate body
          if(empty($data['body'])){
            $data['body_err'] = 'Please add body text.';
          }
          //Make sure no errors
          if(empty($data['title_err']) && empty($data['body_err'])){
            // validated
            if($this->postModel->addPost($data)){
              flash('post_message', 'Postr added');
              redirect('posts');
            } else {
              die('Something went wrong');
            }
  
          } else {
            // load view with errors
            $this->view('posts/add', $data);
  
          }
  
          
      
        } else {

          $data = [
            'title' => '',
            'body' => ''
          ];
          $this->view('posts/add', $data);
        }
        

      } else {
        flash('register_success', 'Please log in, to write a post.', 'alert alert-danger');
        redirect('users/login');
      }
    }

    public function edit($id){
      //if(isLoggedIn()){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
          // add posts
          // sanitaze POST array
          
          $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

          $data = [
            'id' => $id,
            'title' => trim($_POST['title']),
            'body' => trim($_POST['body']),
            'user_id' => trim($_SESSION['user_id']),
            'title_err' => '',
            'body_err' => ''
          ];
          //validate title
          if(empty($data['title'])){
            $data['title_err'] = 'Please add title.';
          }
  
          //validate body
          if(empty($data['body'])){
            $data['body_err'] = 'Please add body text.';
          }
          //Make sure no errors
          if(empty($data['title_err']) && empty($data['body_err'])){
            // validated
            if($this->postModel->updatePost($data)){
              flash('post_message', 'Post updated');
              redirect('posts');
            } else {
              die('Something went wrong');
            }
  
          } else {
            // load view with errors
            $this->view('posts/edit', $data);
  
          }     
        } else {
          //get exixting post from model
          $post = $this->postModel->getPostById($id);

          // check for owner
          if($post->user_id != $_SESSION['user_id']){
            redirect('posts');
          }
          $data = [
            'id' => $id,
            'title' => $post->title,
            'body' => $post->body
          ];
          $this->view('posts/edit', $data);
        }
      // } else {
      //   flash('register_success', 'Please log in, to write a post.', 'alert alert-danger');
      //   redirect('users/login');
      // }
    }

    public function show($id){
      $post = $this->postModel->getPostById($id);
      $user = $this->userModel->getUserById($post->user_id);
      $data = [
        'post' => $post,
        'user' => $user
      ];
      $this->view('posts/show', $data);

    }

    public function delete($id){
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if($this->postModel->deletePost($id)){
          flash('post_message', 'Post deleted');
          redirect('posts');
        } else {
          die('Something went wrong');
        }
      } else {
        redirect('posts');
      }
    }
  }