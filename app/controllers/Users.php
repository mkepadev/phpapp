<?php

  class Users extends Controller{

    public function __construct(){
      $this->userModel = $this->model('User');
    }

    public function register(){
      // check for POST
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // process form

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $data = [
          'name' => trim($_POST['name']),
          'email' => trim($_POST['email']),
          'password' => trim($_POST['password']),
          'confirm_password' => trim($_POST['confirm_password']),
          'name_err' => '',
          'email_err' => '',
          'password_err' => '',
          'confirm_password_err' => ''
        ];

        // validate name
        if(empty($data['name'])){
          $data['name_err'] = 'Please enter name';
        }

        // validate email
        if(empty($data['email'])){
          $data['email_err'] = 'Please enter email';
        } else {
          // check email
          if($this->userModel->findUserByEmail($data['email'])){
            $data['email_err'] = 'Email is already taken';
          }
        }


        // validate password
        if(empty($data['password'])){
          $data['password_err'] = 'Please enter password';
        } elseif(strlen($data['password']) < 6) {
          $data['password_err'] = 'Password must be at least 6 characters';
        }

         // validate confirm password
        if(empty($data['confirm_password'])){
          $data['confirm_password_err'] = 'Please confirm password';
        } else {
          if($data['password'] != $data['confirm_password']){
            $data['confirm_password_err'] = 'Passwords do not match';
          }
        }

        // check if all error messages are empty
        if(empty($data['name_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])){
          // Validated
          
          //hash password
          $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

          // register user
          if($this->userModel->register($data)){
            flash('register_success', 'You are registered and you can log in.');
            redirect('users/login');
          } else {
            die('Something went wrong.');
          }

        } else {
          $this->view('users/register', $data);
        }


      } else{
        //init data
        $data = [
          'name' => '',
          'email' => '',
          'password' => '',
          'confirm_password' => '',
          'name_err' => '',
          'email_err' => '',
          'password_err' => '',
          'confirm_password_err' => '',
        ];

        // load view
        $this->view('users/register', $data);
      }
    }
    public function login(){
      // check for POST
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // process form

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $data = [
          'email' => trim($_POST['email']),
          'password' => trim($_POST['password']),
          'email_err' => '',
          'password_err' => '',
        ];

        // validate email
        if(empty($data['email'])){
          $data['email_err'] = 'Please enter email';
        }

        // validate password
        if(empty($data['password'])){
          $data['password_err'] = 'Please enter password';
        }

        if($this->userModel->findUserByEmail($data['email'])){
          // user found
        } else {
          $data['email_err'] = 'User does not exist';
        }

        // check if all error messages are empty
        if(empty($data['email_err']) && empty($data['password_err'])){
          // Validated
          // check and set logged in user
          $loggedInUser = $this->userModel->login($data['email'], $data['password']);

          if($loggedInUser){
            // create session
            $this->createUserSession($loggedInUser);

            
          } else{
            $data['password_err'] = 'Password incorect';
            $this->view('users/login', $data);
          }

        } else {
          $this->view('users/login', $data);
        }

      } else{
        //init data
        $data = [
          'email' => '',
          'password' => '',
          'email_err' => '',
          'password_err' => '',
        ];

        // load view
        $this->view('users/login', $data);
      }
    }

    public function createUserSession($user){
      $_SESSION['user_id'] = $user->id;
      $_SESSION['user_name'] = $user->name;
      $_SESSION['user_email'] = $user->email;

      redirect('posts');
    }

    public function logout(){
      unset($_SESSION['user_id']);
      unset($_SESSION['user_email']);
      unset($_SESSION['user_name']);
      redirect('');
    }
  }