<?php

  // Database params

  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', 'root');
  define('DB_NAME', 'shareposts');

  // app root
  define('APPROOT', dirname(dirname(__FILE__)));

  // url root -- change to yours
  define('URLROOT', 'http://localhost/phpapp');

  // site name
  define('SITENAME', 'AppMVC');