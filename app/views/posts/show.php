<?php require APPROOT . '/views/inc/header.php';?>
<div class="row">
  <div class="col-md-6 mb-3">
  <a href="<?php echo URLROOT; ?>/posts" class="btn btn-light"><i class="fa fa-backward"></i> Back</a> 
  </div>
</div>

  <div class="card mb-3 p-2">  
    <div class="card-body ">
      <h1 class="card-title">
        <?php echo $data['post']->title;?>
      </h1>
      <div class=" mb-1 d-inline-block text-muted">
      <small>Written by: <?php echo $data['user']->name; ?> on <?php echo $data['post']->created_at; ?></small>
</div>
      <p class="card-text"><?php echo $data['post']->body; ?></p>
      
    </div>
  </div>
  <?php 
    if(isLoggedIn() && $data['post']->user_id == $_SESSION['user_id']) : ?>
    <a href="<?php echo URLROOT;?>/posts/edit/<?php echo $data['post']->id; ?>" class="btn btn-dark">Edit</a>
    <form action="<?php echo URLROOT; ?>/posts/delete/<?php echo $data['post']->id; ?>" method="post">
    <input type="submit" value="Delete" class="btn btn-danger">
  </form>
  <?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php';?>