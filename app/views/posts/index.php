<?php require APPROOT . '/views/inc/header.php';?>
<div class="row">
  <div class="col-md-6">
    <h1>Posts</h1>
    
  </div>
  <div class="col-md-6">
  <?php //if(isLoggedIn()) : ?>
    <a href="<?php echo URLROOT;?>/posts/add" class="btn btn-primary pull-right">
    <i class="fa fa-pencil"></i> Add post
    </a>
  <?php //endif; ?>  
  </div>
</div>
<?php if(($data['countPosts'] != 0)) : ?>
  <?php foreach ($data['posts'] as $post) : ?>
  <div class="card mb-3 p-2">  
    <div class="card-body ">
      <h4 class="card-title">
        <?php echo $post->title;?>
      </h4>
      <div class=" mb-1 d-inline-block text-muted">
      <small>Written by: <?php echo $post->name; ?> on <?php echo $post->postCreated; ?></small>
</div>
      <p class="card-text"><?php echo $post->body; ?></p>
      <a href="<?php echo URLROOT; ?>/posts/show/<?php echo $post->postId; ?>" class="btn btn-success btn-sm">More</a>
    </div>
  </div>  
  <?php endforeach; ?>
<?php else :?>
<div class="card card-body">
  <h4 class="card-title">There's no posts</h4>
  <p>Please <a class="link" href="<?php echo URLROOT; ?>/users/login">login</a> or <a href="<?php echo URLROOT; ?>/users/register">register</a> to write first post.</p> 
</div>
<?php endif;?>

<?php require APPROOT . '/views/inc/footer.php';?>